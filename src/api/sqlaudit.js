import request from '@/utils/request'

// database增删查改接口
export function getDatabasesList(params) {
  return request({
    url: '/database/',
    method: 'get',
    params
  })
}

export function createDatabase(data) {
  return request({
    url: '/database/',
    method: 'post',
    data
  })
}

export function updateDatabase(id, data) {
  return request({
    url: `/database/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteDatabase(id) {
  return request({
    url: `/database/${id}/`,
    method: 'delete'
  })
}

// 获取组内审核人
export function getTreatersList(data) {
  return request({
    url: `/treaters/`,
    method: 'post',
    data
  })
}

// SQL提交时进行检查，返回检查状态
export function SubmitCheckSQL(data) {
  return request({
    url: `/sqlcheck/`,
    method: 'post',
    data
  })
}

// SQL审核列表
export function getSQLList(params) {
  return request({
    url: `/sqlaudit/`,
    method: 'get',
    params
  })
}

// SQL审核动作
export function auditSQLAction(id, action) {
  return request({
    url: `/sqlaudit/${id}/${action}`,
    method: 'get'
  })
}
