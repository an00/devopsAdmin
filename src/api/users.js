import request from '@/utils/request'
// 用户和用户组api请求定义

export function getUserList(params) {
  return request({
    url: '/users/',
    method: 'get',
    params
  })
}

export function getOneUser(id, params) {
  return request({
    url: `/users/${id}/`,
    method: 'get',
    params
  })
}

export function deleteUser(uid) {
  return request({
    url: `/users/${uid}/`,
    method: 'delete'
  })
}

export function updateUser(uid, data) {
  return request({
    url: `/users/${uid}/`,
    method: 'patch',
    data
  })
}

// 创建用户
// export function createUser(data) {
//   return request({
//     url: '/register/',
//     method: 'post',
//     data
//   })
// }
export function createUser(data) {
  return request({
    url: '/users/',
    method: 'post',
    data
  })
}

export function getGroupList(params) {
  return request({
    url: '/groups/',
    method: 'get',
    params
  })
}

export function createGroup(data) {
  return request({
    url: '/groups/',
    method: 'post',
    data
  })
}

export function updateGroup(id, data) {
  return request({
    url: `/groups/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteGroup(id) {
  return request({
    url: `/groups/${id}/`,
    method: 'delete'
  })
}

// 组成员列表
export function getGroupMembers(gid, params) {
  return request({
    url: `/groupmembers/${gid}/`,
    method: 'get',
    params
  })
}

// 删除组成员，data需要user id
export function deleteGroupMember(gid, data) {
  return request({
    url: `/groupmembers/${gid}/`,
    method: 'delete',
    data
  })
}

// 用户所属组列表
export function getUserGroups(uid, params) {
  return request({
    url: `/usergroups/${uid}/`,
    method: 'get',
    params
  })
}

// 删除用户所属组，data需要group id
export function updateUserGroups(uid, data) {
  return request({
    url: `/usergroups/${uid}/`,
    method: 'put',
    data
  })
}

// 接口权限列表
export function getPermissionsList(params) {
  return request({
    url: `/permissions/`,
    method: 'get',
    params
  })
}

// 获取用户组的前端菜单
export function getGroupMenuList(id, params) {
  return request({
    url: `/groupmenus/${id}/`,
    method: 'get',
    params
  })
}

// 给指定用户组增加新的前端菜单
export function updateGroupMenu(id, data) {
  return request({
    url: `/groupmenus/${id}/`,
    method: 'put',
    data
  })
}
