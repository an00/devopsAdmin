// import { login, logout, getInfo } from '@/api/login'
import { login, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    envs: [ // 环境列表
      { value: 'dev', text: '开发环境', color: 'success' },
      { value: 'test', text: '测试环境', color: 'primary' },
      { value: 'prod', text: '生产环境', color: 'danger' }
    ]
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    // 前端带单
    SET_MENUS: (state, menus) => {
      state.menus = menus
    },
    SET_USERID: (state, id) => {
      state.userid = id
    },
    SET_SUPER: (state, superuser) => {
      state.superuser = superuser
    },
    SET_ROLE: (state, role) => {
      state.userrole = role
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          setToken(response.token)
          commit('SET_TOKEN', response.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          commit('SET_MENUS', response.menus)
          commit('SET_NAME', response.name)
          commit('SET_USERID', response.id)
          commit('SET_SUPER', response.superuser)
          commit('SET_ROLE', response.role)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出，暂时不对后端请求，前端删除JWT
    LogOut({ commit, state }) {
      commit('SET_TOKEN', '')
      removeToken()
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
