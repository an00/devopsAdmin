import request from '@/utils/request'

// 获取任务列表
export function getTaskList(params) {
  return request({
    url: '/tasks/',
    method: 'get',
    params
  })
}

// 创建任务
export function createTask(data) {
  return request({
    url: '/tasks/',
    method: 'post',
    data
  })
}

// 执行任务
export function updateTask(id, data) {
  return request({
    url: '/tasks/' + id + '/',
    method: 'patch',
    data
  })
}

// 任务详情
export function detailTask(id) {
  return request({
    url: '/tasks/' + id + '/',
    method: 'get'
  })
}
