# OM运维系统
> 项目基于极简的vue-admin模板 [PanJiaChen/vueAdmin-template](https://github.com/PanJiaChen/vueAdmin-template) ，制作的后台管理页面。

示例页面 [om.an00.cn](http://om.an00.cn/) 登陆请点help查看密码。

Python项目 [an00/devops](https://gitee.com/an00/devops) 的前端界面，REST API请参考它。

## 依赖
先安装 `node.js==8.x` LTS版本。


## 调试步骤

启动本项目需要先将后端run起来

```shell
git clone https://gitee.com/an00/devopsAdmin.git

cd devopsAdmin

npm install --registry=https://registry.npm.taobao.org

npm run dev
```