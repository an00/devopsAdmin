const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  // avatar: state => state.user.avatar,
  name: state => state.user.name,
  menus: state => state.user.menus,
  userid: state => state.user.userid,
  superuser: state => state.user.superuser,
  userrole: state => state.user.userrole,
  envs: state => state.user.envs
}
export default getters
