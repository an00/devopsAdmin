import request from '@/utils/request'

export function getWorkOrderList(params) {
  return request({
    url: '/workorder/',
    method: 'get',
    params
  })
}

export function updateWorkOrder(id, data) {
  return request({
    url: `/workorder/${id}/`,
    method: 'patch',
    data
  })
}

export function createWorkOrder(data) {
  return request({
    url: `/workorder/`,
    method: 'post',
    data
  })
}

export function uploadImageFile(filename, params) {
  return request({
    url: `/upload/${filename}`,
    method: 'put',
    data: params
    // config: {
    //   headers: {
    //     'Content-Type': 'multipart/form-data'
    //   }
    // }
  })
}
