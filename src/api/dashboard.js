import request from '@/utils/request'

export function getDashboardInfo() {
  return request({
    url: '/dashboard/',
    method: 'get'
  })
}
