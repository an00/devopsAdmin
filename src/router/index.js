import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard', noCache: true }
    }]
  },
  {
    path: '/monitor',
    component: Layout,
    redirect: 'noredirect',
    meta: { title: '监控管理', icon: 'monitor' },
    children: [
      {
        path: 'zabbix',
        name: 'zabbix',
        component: () => import('@/views/monitor/zabbix/index'),
        meta: { title: 'Zabbix', icon: 'z' }
      },
      {
        path: 'chart',
        name: 'chart',
        component: () => import('@/views/monitor/chart/index'),
        meta: { title: '常用图表', icon: 'chart' }
      }
    ]
  },
  {
    path: '/resources',
    redirect: 'noredirect',
    component: Layout,
    meta: { title: '资源管理', icon: 'nested' },
    children: [
      {
        path: 'idcs',
        name: 'idcs',
        component: () => import('@/views/resources/idcs/index'),
        meta: { title: '机房管理', icon: 'idc' }
      },
      {
        path: 'cabinets',
        name: 'cabinet',
        component: () => import('@/views/resources/cabinet/index'),
        meta: { title: '机柜管理', icon: 'cabinet' }
      },
      {
        path: 'servers',
        name: 'server',
        component: () => import('@/views/resources/server/index'),
        meta: { title: '服务器', icon: 'server' }
      },
      {
        path: 'business',
        name: 'business',
        component: () => import('@/views/resources/business/index'),
        meta: { title: '业务线', icon: 'business' }
      }
    ]
  },
  {
    path: '/mysql',
    component: Layout,
    redirect: 'noredirect',
    meta: { title: 'SQL上线', icon: 'mysql' },
    children: [
      {
        path: 'edit',
        name: 'SQL提交',
        component: () => import('@/views/sqlaudit/edit'),
        meta: { title: 'SQL提交', icon: 'edit' }
      },
      {
        path: 'audit',
        name: 'SQL审核',
        component: () => import('@/views/sqlaudit/audit'),
        meta: { title: 'SQL审核', icon: 'audit' }
      },
      {
        path: 'database',
        name: '数据库',
        component: () => import('@/views/sqlaudit/databaseConfig'),
        meta: { title: '数据库配置', icon: 'database' }
      }
    ]
  },
  {
    path: '/workorder',
    component: Layout,
    redirect: 'noredirect',
    meta: { title: '工单管理', icon: 'workorder' },
    children: [
      {
        path: 'edit',
        name: '提交工单',
        component: () => import('@/views/workorder/edit/index'),
        meta: { title: '提交工单', icon: 'edit' }
      },
      {
        path: 'handle',
        name: '处理工单',
        component: () => import('@/views/workorder/handle/index'),
        meta: { title: '处理工单', icon: 'audit' }
      },
      {
        path: 'history',
        name: '历史工单',
        component: () => import('@/views/workorder/history/index'),
        meta: { title: '历史工单', icon: 'history' }
      }
    ]
  },
  // {
  //   path: '/tasks',
  //   component: Layout,
  //   name: '任务系统',
  //   meta: { title: '任务系统', icon: 'tree' },
  //   children: [
  //     {
  //       path: 'add',
  //       name: '任务添加',
  //       component: () => import('@/views/tasks/add/index'),
  //       meta: { title: '任务添加', icon: 'form' }
  //     },
  //     {
  //       path: 'list',
  //       name: '任务列表',
  //       component: () => import('@/views/tasks/list/index'),
  //       meta: { title: '任务列表', icon: 'table' }
  //     }
  //   ]
  // },

  {
    path: '/roles',
    component: Layout,
    redirect: 'noredirect',
    meta: { title: '角色管理', icon: 'peoples' },
    children: [
      {
        path: 'users',
        name: '用户列表',
        component: () => import('@/views/users/index'),
        meta: { title: '用户列表', icon: 'user2' }
      },
      {
        path: 'group',
        name: '用户组',
        component: () => import('@/views/users/groups'),
        meta: { title: '用户组', icon: 'group' }
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

