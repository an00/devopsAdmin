import request from '@/utils/request'

// idc增删查改接口
export function getIdcsList(params) {
  return request({
    url: '/idcs/',
    method: 'get',
    params
  })
}

export function createIdc(data) {
  return request({
    url: '/idcs/',
    method: 'post',
    data
  })
}

export function updateIdc(id, data) {
  return request({
    url: `/idcs/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteIdc(id) {
  return request({
    url: `/idcs/${id}/`,
    method: 'delete'
  })
}

// cabinet增删查改接口
export function getCabinetsList(params) {
  return request({
    url: '/cabinet/',
    method: 'get',
    params
  })
}

export function createCabinet(data) {
  return request({
    url: '/cabinet/',
    method: 'post',
    data
  })
}

export function updateCabinet(id, data) {
  return request({
    url: `/cabinet/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteCabinet(id) {
  return request({
    url: `/cabinet/${id}/`,
    method: 'delete'
  })
}

// server增删查改接口
export function getServersList(params) {
  return request({
    url: '/server/',
    method: 'get',
    params
  })
}

export function updateServer(id, data) {
  return request({
    url: `/server/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteServer(id, data) {
  return request({
    url: `/server/${id}/`,
    method: 'delete',
    data
  })
}

// 获取business业务线关系树接口
export function getBusinessTree() {
  return request({
    url: '/businessmanage/',
    method: 'get'
  })
}

export function getBusinessList(params) {
  return request({
    url: '/business/',
    method: 'get',
    params
  })
}

// 获取单个业务线详细信息
export function getBusinessInfo(id) {
  return request({
    url: `/business/${id}/`,
    method: 'get'
  })
}

export function createBusiness(data) {
  return request({
    url: '/business/',
    method: 'post',
    data
  })
}

export function updateBusiness(id, data) {
  return request({
    url: `/business/${id}/`,
    method: 'put',
    data
  })
}
export function deleteBusiness(id) {
  return request({
    url: `/business/${id}/`,
    method: 'delete'
  })
}
